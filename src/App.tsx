import React, {useRef, useState} from 'react';
import './App.css';
import useDancefloorConfig from "./hooks/useDancefloorConfig";
import {defaultDancefloorDimensions} from "./constants";
import {DancefloorDimensions} from "./api/types";
import Dancefloor from "./components/Dancefloor";
import Controls from "./components/Controls";

function App() {
    const [dimensions, setDimensions] = useState<DancefloorDimensions>(defaultDancefloorDimensions);
    const {loading, error, dancefloorConfig} = useDancefloorConfig(dimensions);
    const rowsField = useRef<HTMLInputElement>(null);
    const colsField = useRef<HTMLInputElement>(null);

    const handleGenerate = (): void => {
        setDimensions({
            rows: Number(rowsField.current?.value),
            cols: Number(colsField.current?.value),
        });
    }

    return (
        <div className="App">
            <div style={{ flexGrow: 0 }}>
                <Controls
                    rowsFieldRef={rowsField}
                    colsFieldRef={colsField}
                    handleGenerate={handleGenerate}
                />
            </div>
            {loading && <div>"Loading..."</div>}
            {error && <div>{error}</div>}
            <div style={{ flexGrow: 1, display: "flex" }}>
                {dancefloorConfig && <Dancefloor config={dancefloorConfig}/>}
            </div>
        </div>
    );
}

export default App;
