import { getDancefloorConfig } from "./getDancefloorConfig";

describe("API getDancefloorConfig", () => {
    // it("should call proper API method", async () => {
    // typically it should use mocked fetch and check if proper API endpoint is called with a valid params
    it("should asynchronously return generic value", async () => {
       const dancefloorConfig = await getDancefloorConfig({ rows: 2, cols: 3})
       expect(dancefloorConfig.length).toBe(2);
       expect(dancefloorConfig[0].length).toBe(3);
       expect(dancefloorConfig[1].length).toBe(3);
       expect(dancefloorConfig[1][2].color).toEqual(expect.stringMatching(/^rgb\(\d+, \d+, \d+\)$/));
   }) ;
});