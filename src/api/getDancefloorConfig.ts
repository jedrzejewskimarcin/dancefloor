import {DancefloorConfig, DancefloorDimensions} from "./types";

const randomColor = () => {
    const randomNumberFrom0To255 = (): string => Number(Math.random()*255).toFixed(0);
    return `rgb(${randomNumberFrom0To255()}, ${randomNumberFrom0To255()}, ${randomNumberFrom0To255()})`;
}

export const getDancefloorConfig = async (dimensions: DancefloorDimensions): Promise<DancefloorConfig> => {
    const dancefloorConfig: DancefloorConfig = [];
    for (let row = 0; row < dimensions.rows; row++) {
        dancefloorConfig.push([])
        for (let col = 0; col < dimensions.cols; col++) {
            dancefloorConfig[row].push({ color: randomColor()})
        }
    }
    return new Promise(resolve => setTimeout(() => resolve(dancefloorConfig), 300));
}