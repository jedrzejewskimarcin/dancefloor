export type DancefloorDimensions = {
    rows: number;
    cols: number;
};

export type DancefloorField = {
    color: string;
}

export type DancefloorConfig = DancefloorField[][];