import React, {Ref, FC, FormEvent} from "react";
import "./Controls.css";

const Controls: FC<{
    rowsFieldRef: Ref<HTMLInputElement>;
    colsFieldRef: Ref<HTMLInputElement>;
    handleGenerate: () => void;
}> = ({rowsFieldRef, colsFieldRef, handleGenerate}) => {
    const handleSubmit = (event: FormEvent<HTMLFormElement>): void => {
        event.preventDefault();
        handleGenerate();
    }
    return (
        <form onSubmit={handleSubmit}>
            <input type="number" min={1} step={1} placeholder="Enter rows number" ref={rowsFieldRef}/>
            <input type="number" min={1} step={1} placeholder="Enter cols number" ref={colsFieldRef}/>
            <input type="submit" value="Generate" />
        </form>
    );
};

export default Controls;