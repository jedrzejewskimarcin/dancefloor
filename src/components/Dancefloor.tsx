import React, {FC, ReactNodeArray, useRef, useState} from "react";
import {DancefloorConfig} from "../api/types";
import {Rect, Stage, Layer} from "react-konva";
import Konva from "konva";

const Dancefloor: FC<{ config: DancefloorConfig }> = ({config}) => {
    const wrapper = useRef<HTMLDivElement>(null);
    const [activeFields, setActiveFields] = useState<{ [key: string]: boolean }>({});

    const totalRows = config.length;
    const totalCols = config[0].length;

    const scaleX = wrapper.current ? (wrapper.current.offsetWidth / totalCols) : 0;
    const scaleY = wrapper.current ? (wrapper.current.offsetHeight / totalRows) : 0;
    const scale = Math.min(scaleX, scaleY);

    const width = totalCols * scale;
    const height = totalRows * scale;

    const handlePress = (event: Konva.KonvaEventObject<MouseEvent> | Konva.KonvaEventObject<TouchEvent>) => {
        if(!(event.evt as MouseEvent).buttons) return;
        setActiveFields({
            ...activeFields,
            [event.target.attrs.dataKey]: true,
        })
    };
    const handleRelease = (event: Konva.KonvaEventObject<MouseEvent> | Konva.KonvaEventObject<TouchEvent>) => {
        setActiveFields({
            ...activeFields,
            [event.target.attrs.dataKey]: false,
        })
    };

    const rects = config.reduce((all: ReactNodeArray, row, rowIndex) => {
        all.push(
            ...row.map((field, colIndex) => (
                <Rect
                    x={colIndex}
                    y={rowIndex}
                    width={1}
                    height={1}
                    key={`${rowIndex}-${colIndex}`}
                    dataKey={`${rowIndex}-${colIndex}`}
                    fill={field.color}
                    onMouseDown={handlePress}
                    onMouseEnter={handlePress}
                    onTouchStart={handlePress}
                    onMouseUp={handleRelease}
                    onMouseLeave={handleRelease}
                    onTouchEnd={handleRelease}
                    opacity={activeFields[`${rowIndex}-${colIndex}`] ? 1 : 0.5}
                />))
        );
        return all;
    }, []);

    return (
        <div ref={wrapper} style={{ flexGrow: 1 }}>
            <div style={{width, height, margin: "auto"}}>
            <Stage width={width} height={height} scale={{x: scale, y: scale}}>
                <Layer>
                    {rects}
                </Layer>
            </Stage>
            </div>
        </div>
    );
}

export default Dancefloor;