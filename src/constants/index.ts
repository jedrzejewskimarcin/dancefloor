import {DancefloorDimensions} from "../api/types";

export const defaultDancefloorDimensions: DancefloorDimensions = {
    rows: 3,
    cols: 4,
}