import {useEffect, useState} from "react";
import {DancefloorConfig, DancefloorDimensions} from "../api/types";
import {getDancefloorConfig} from "../api/getDancefloorConfig";

export type UseDancefloorConfigResult = {
    loading: boolean;
    error? : string;
    dancefloorConfig?: DancefloorConfig;
}

const useDancefloorConfig = (dimensions: DancefloorDimensions): UseDancefloorConfigResult => {
    const [loading, setLoading] = useState<boolean>(false);
    const [error, setError] = useState<string>();
    const [dancefloorConfig, setDancefloorConfig] = useState<DancefloorConfig>();

    useEffect(() => {
        setLoading(true);
        getDancefloorConfig(dimensions)
            .then(setDancefloorConfig)
            .catch(error => setError(error.message))
            .finally(() => setLoading(false));
    }, [dimensions]);

    return {
        loading,
        error,
        dancefloorConfig
    };
}

export default useDancefloorConfig;